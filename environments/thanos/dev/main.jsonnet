local common = import 'common.libsonnet';
local thanos = import 'thanos/thanos.libsonnet';
local tk = import 'tk';

// You might be reading this file in order to copy-paste it into a real
// environment. If so, please configure resources on all the things!
// `thanos.operator.new` takes an optional resources parameter, to start with.
//
// It's recommended to set the memory limit and request to equal values. This
// limits the possibility of memory overcommit for nodes, and the associated pod
// OOMKills that accompany that state.
//
// It's recommended to set a CPU request, but not a limit. Requests are
// implemented as CPU cgroup shares, which controls the ratio of cycles
// available to processes when the system is under CPU pressure. CPU limits are
// implemented as cgroup CFS throttling, which controls the absolute proportion
// of time the process is allowed to execute on the CPU. This can cause
// un-necessary throttling during normal operation.


(import 'thanos/crds.libsonnet') +
(import 'thanos/operator-role.libsonnet') +
{
  namespace: common.namespace('thanos'),
  operator: thanos.operator.new(env=tk.env.metadata.labels.name) + {
    deployment+: {
      spec+: {
        replicas: 1,
      },
    },
  },

  thanos: thanos.thanos.new(env=tk.env.metadata.labels.name),
}
