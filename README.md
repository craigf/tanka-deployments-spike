# tanka-deployments

A monorepo for Kubernetes deployment declarations using
[tanka](https://tanka.dev/).

## Contributing

### Dependencies

1. tanka
1. jsonnet-bundler
1. kubectl

`.tool-versions` (used by [`asdf`](https://github.com/asdf-vm/asdf)) and
`.gitlab-ci.yml` should be seen as a source of truth for versions.

### Repository structure

The top-level directory mainly consists of a single tanka structure created with
`tk init`.

There are two levels of tanka environments in the environments directory: the
first level contains different groups of software (e.g. thanos, prometheus), and
the second level are environments of these deployments.

### Creating a new deployment / environment

```
tk env add environments/<software>/<environment> --namespace <namespace> \
  --server-from-context <kube context>
```

For example:

```
tk env add environments/thanos/gprd --namespace thanos \
  --server-from-context gprd
```

See `tk env add --help` for full details. If you don't have the relevant server
in your local kubectl context, you can pass `--server` instead to set the API
server to an arbitrary string.

Check the generated `spec.json` in the new environment's directory. You might
wish to add metadata.labels, in order to later reference these for resource
labels. `environments/tanka/dev` does this, because `tk.env.metadata.name`
contains a fully-qualified name containing a leading `environments/`.

It is the environment author's responsibility to ensure that the specified
namespace is created. Tanka can do this: see `environments/thanos/dev` for an
example which calls `lib/common.namespace.new`.

You can now run `tk` eval/show/diff/apply commands as you wish for that
environment.

### The dev environment

The `dev` environment for each tanka deployment is special, in that its API
server is not a globally routable shared resource. You should use this
environment to test resource configuration against a local Kubernetes
environment, e.g. minikube, k3d, or KinD. Unfortunately, some of these tools
reserve a random port for the API server. Tanka doesn't support overriding the
API server on the command line, so you'll likely need to override this and
ignore the git diff. Example:

```
tk env set environments/thanos/dev --server-from-context minikube
```
