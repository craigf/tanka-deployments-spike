local k = import 'ksonnet-util/kausal.libsonnet';

{
  // Creates a new namespace containing a "name" label, in the same way that
  // helm does. This might not be necessary but is good for consistency, and
  // avoids confusing when making label queries.
  namespace(name):: k.core.v1.namespace.new(name) + {
    metadata+: {
      labels+: {
        name: name,
      },
    },
  },
}
