local k = import 'ksonnet-util/kausal.libsonnet';

local deployment = k.apps.v1.deployment;
local container = k.core.v1.container;
local port = k.core.v1.containerPort;

local operatorProbe = {
  httpGet: {
    path: '/metrics',
    port: 'metrics',
  },
};

{
  operator:: {
    new(name='thanos-operator', env, resources={})::
      {
        deployment: deployment.new(
          name=name,
          replicas=2,
          containers=[
            container.new(name='operator', image='banzaicloud/thanos-operator:0.1.0')
            + container.withPorts(port.new('metrics', 8080))
            + {
              livenessProbe: operatorProbe,
              readinessProbe: operatorProbe,
              resources: resources,
            },
          ],
          podLabels={
            env: env,
          },
        ) + {
          spec+: {
            template+: {
              spec+: {
                serviceAccountName: name,
              },
            },
          },
        },

        service: k.util.serviceFor(self.deployment) + {
          metadata+: {
            labels+: {
              env: env,
            },
          },
          spec+: {
            clusterIP: 'None',
          },
        },

        serviceAccount: k.core.v1.serviceAccount.new(name) + {
          metadata+: {
            labels+: {
              name: name,
              env: env,
            },
          },
        },

        clusterRoleBinding: {
          apiVersion: 'rbac.authorization.k8s.io/v1',
          kind: 'ClusterRoleBinding',
          metadata: {
            name: name,
            labels: {
              name: name,
              env: env,
            },
          },
          roleRef: {
            apiGroup: 'rbac.authorization.k8s.io',
            kind: 'ClusterRole',
            name: name,
          },
          subjects: [
            {
              kind: 'ServiceAccount',
              name: name,
              namespace: 'thanos',
            },
          ],
        },
      },
  },

  thanos: {
    new(name='thanos', env)::
      {
        apiVersion: 'monitoring.banzaicloud.io/v1alpha1',
        kind: 'Thanos',
        metadata: {
          name: name,
          labels: {
            name: name,
            env: env,
          },
        },
        spec: {
          storeGateway: {},
        },
      },
  },
}
